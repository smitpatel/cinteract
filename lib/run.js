var spawn = require('child_process').spawn
  , fs = require('fs')
  , socketio = require('socket.io')
  , CStream = require('../lib/CStream').CStream;


var TMPPATH = "/tmp/";
var io;

exports.run = function(socket, codeId) {
    var stream = new CStream();
    var proc = spawn(__dirname + '/run.py',['/usr/bin/stdbuf','-o0',TMPPATH+codeId]);

    stream.pipe(proc.stdin);

    proc.stdout.on('data', function(data) {
      socket.send(data.toString());
    });

    proc.stderr.on('data', function(data) {
      socket.send(data.toString());
    });

    socket.on('message', function(data) {
      stream.write(data);
    });

    proc.on('exit', function(code) {
      socket.emit('exit', code);
      console.log('program ended ', code);
    });

}