var bcrypt = require('bcrypt'),
    User = require('../models/user.js');

exports.auth = function (email, password, callback) {
    User.findOne({email: email}, function (err, user) {
        if (err) {
            return callback(false, err);
        } else if (!user) {
            callback(false, null);
        } else {
            bcrypt.compare(password.toString(), user.password.toString(), function (err, res) {
                if (res) {
                    callback(true, user);
                } else {
                    callback(false, null);
                }
            });
        }
    });
}

exports.getPasswordHash = function (password, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(password, salt, function(err, hash) {
            callback(hash);
        });
    });
}
