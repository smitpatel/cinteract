var File = require('file-utils').File,
    fs = require('fs'),
    spawn = require('child_process').spawn;

exports.compile = function (req, res) {
    var settings = {
        suffix: ".c",
        directory: "/tmp"
    };

    if (req.body.src) {
        File.createTempFile(settings, function (err, file) {
            if (err) {
                // console.log("Error creating file: " + err);
                // TODO: use logger
            } else {
                // console.log("File created: " + file.toString());
                // TODO: Use logger
            }

            fs.writeFile(file.toString(), req.body.src, function (err) {
                if (err) {
                    // console.log(err);
                    // TODO: Use logger
                } else {
                    var objFile = file.toString().substr(0, file.toString().lastIndexOf('.')),
                        op = "",
                        proc = spawn('gcc', ['-Wall', '-o', objFile, file.toString()]);

                    proc.stdout.on('data', function (data) {
                        op += data;
                    });

                    proc.stderr.on('data', function (data) {
                        op += data;
                    });

                    proc.on('exit', function (code) {
                        res.send({
                            exit_code: code,
                            compile_op: op,
                            executable: objFile.substr(objFile.lastIndexOf('/') + 1, objFile.length)
                        });
                    });
                }
            });//, function(err) {
        });
    } else {
        res.send({
            exit_code: 1,
            error: "Empty source"
        });
    }

};

