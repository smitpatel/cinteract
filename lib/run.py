#!/usr/bin/python
import os
import sys
from sandbox import *

system, machine = os.uname()[0], os.uname()[4]

def main(args):
    # sandbox configuration
    cookbook = {
        'args': args[1:],               # targeted program
        'stdin': sys.stdin,             # input to targeted program
        'stdout': sys.stdout,           # output from targeted program
        'stderr': sys.stderr,           # error from targeted program
        'quota': dict(wallclock = 10000, # 10 sec
                      cpu = 25,         # 25 msec
                      memory = 8388608, #  8 MB
                      disk = 1048576)}  #  1 MB
    # create a sandbox instance and execute till end
    msb = MiniSandbox(**cookbook)
    msb.run()
    return os.EX_OK

# mini sandbox with embedded policy
class MiniSandbox(SandboxPolicy,Sandbox):
    sc_table = None
    sc_safe = dict(i686 = set([3, 4, 19, 45, 54, 90, 91, 122, 125, 140, 163, \
        192, 197, 224, 243, 252, ]), x86_64 = set([0, 1, 5, 8, 9, 10, 11, 12, \
        16, 25, 63, 158, 231, ]), ) # white list of essential linux syscalls
    def __init__(self, *args, **kwds):
        # initialize table of system call rules
        self.sc_table = [self._KILL_RF, ] * 1024
        for scno in MiniSandbox.sc_safe[machine]:
            self.sc_table[scno] = self._CONT
        # initialize as a polymorphic sandbox-and-policy object
        SandboxPolicy.__init__(self)
        Sandbox.__init__(self, *args, **kwds)
        #self.policy = self
    def probe(self):
        # add custom entries into the probe dict
        d = Sandbox.probe(self, False)
        d['cpu'] = d['cpu_info'][0]
        d['mem'] = d['mem_info'][1]
        return d
    def __call__(self, e, a):
        # handle SYSCALL/SYSRET events with local rules
        if e.type in (S_EVENT_SYSCALL, S_EVENT_SYSRET):
            if machine is 'x86_64' and e.ext0 is not 0:
                return self._KILL_RF(e, a)
            return self.sc_table[e.data](e, a)
        # bypass other events to base class
        return SandboxPolicy.__call__(self, e, a)
    def _CONT(self, e, a): # continue
        a.type = S_ACTION_CONT
        return a
    def _KILL_RF(self, e, a): # restricted func.
        a.type, a.data = S_ACTION_KILL, S_RESULT_RF
        return a

if __name__ == "__main__":
    sys.exit(main(sys.argv))
