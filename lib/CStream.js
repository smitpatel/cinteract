var util = require('util');
var Stream = require('stream').Stream;
var events = require('events');

function CStream() {
  this.writable = true;
  this.readable = true;
}

util.inherits(CStream, Stream);

CStream.prototype.write = function (data) {
  this.emit('data', data);
}

CStream.prototype.end = function () {
  this.emit('end');
}

CStream.prototype.destroy = function () {
  this.emit('close');
}

/*CStream.prototype.pipe = function (out) {
    this.on('data', function (data) {
        out.write(data + "\n");
    });
}*/
exports.CStream = CStream;
