var db = require('mongoose'),
    Schema = db.Schema,
    Exercise = require('./exercise.js');


var LevelSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    exercises: [ {
        type: Schema.Types.ObjectId,
        ref: 'Exercise'
    } ]
});

module.exports = db.model('Level', LevelSchema); 
