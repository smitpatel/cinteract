var db = require('mongoose'),
    Schema = db.Schema,
    Level = require('./level.js');

var TopicSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    levels: [Level.schema]
    // TODO: teacher platform -> include creator
    // creator: {
        // type: Schema.Types.ObjectId,
        // ref: 'users'
    // } 
}, { collection: 'topics' });

module.exports = db.model('Topic', TopicSchema);
