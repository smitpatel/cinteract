var db = require('mongoose'),
    Schema = db.Schema;

var UserSchema = new Schema({
    fname: {
        type: String,
        required: true
    },
    lname: {
        type: String,
//         required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
//         required: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.statics.userExists = function(email, callback) {
    this.findOne({email: email}, function (err, user) {
	if (err) {
	    return callback(false, err);
	} else if (user) {
	    callback(true);		//user already exist
	    return true;
	} else {
	    callback(false);
	    return false;
	}
    });
};

module.exports = db.model('User', UserSchema);
