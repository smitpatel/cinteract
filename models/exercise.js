var db = require('mongoose'),
    Schema = db.Schema;

var ExerciseSchema = new Schema({
    contentURL: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    next: {
        url: {
            type: String,
            required: true
        },
        label: {
            type: String,
            required: true
        }
    },
    prev: {
        url: {
            type: String,
            required: true
        },
        label: {
            type: String,
            required: true
        }
    }
}, { collection: 'exercises' });

module.exports = db.model('Exercise', ExerciseSchema);
