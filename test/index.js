var Browser = require("zombie");
var assert = require("assert");

Browser.visit("http://localhost:3000/home", function(e, browser) {
    browser.on('error', function(error) {
        console.log(error);
    });

    assert.ok(browser.query("#compile"));
});
