var express = require('express'),
    ejs = require('ejs'),
    socketio = require('socket.io'),
    passportSIO = require('passport.socketio'),
    http = require('http'),
    nconf = require('nconf'),
    passport = require('passport'),
    PassLocalStr = require('passport-local').Strategy,
    mongoose = require('mongoose'),
    MongoStore = require('connect-mongo')(express),
    expressValidator = require('express-validator'),
    flash = require('connect-flash');

// Local libs
var cbcrypt = require('./lib/cbcrypt.js');

// Routes
var routes = require('./controllers'),
    api = require('./controllers/api');

// Other models
var User = require('./models/user.js');

nconf.argv()
     .env()
     .file({file: 'config/config.json'});

var app = express();
var db = mongoose.connect('mongodb://' + nconf.get('database:host') + '/' + nconf.get('database:name'));

app.configure(function () {
    app.set('port', nconf.get('port'));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(expressValidator);
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ 
        store: new MongoStore({
            db: nconf.get('database:name')
        }),
        secret: nconf.get('session_secret') 
        }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
    app.use(express.compress());
});

//persistent logins

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new PassLocalStr(
    function (email, password, done) {
        process.nextTick(function () {
            cbcrypt.auth(email, password, function (res, user) {
                if (res) {
                    done(null, user);
                } else {
                    done(null, false, { message: 'Incorrect username or password.' });
                }
            });
        });
    })
);

app.configure('development', function () {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.get('/', routes.auth.auth, routes.home);
app.get('/home', routes.auth.auth, routes.home);
app.get('/user', routes.auth.auth, routes.user.user);

app.get('/course', routes.auth.auth, routes.course);
app.get('/templates/:name', routes.auth.auth, routes.templates);

// Profile page
app.get('/part/profile/progress', routes.auth.auth, routes.user.progress);
app.get('/part/profile/badge', routes.auth.auth, routes.user.badge);
app.get('/part/profile/settings', routes.auth.auth, routes.user.settings);
app.get('/part/profile/about', routes.auth.auth, routes.user.about);

// Login
app.get('/login', routes.login);
app.post('/login', passport.authenticate('local', { successRedirect: '/course', failureRedirect: '/login', failureFlash: true }));

// Signup
app.get('/signup', routes.signup.index);
app.post('/signup', routes.signup.Add);

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
});

app.post('/api/compile', routes.auth.authSilent, api.compile);
app.post('/user/checkAvailable', api.checkAvailable);

// json req api
app.get('/api/topic/:tId/level', routes.auth.authSilent, api.tutorial.levelList);
app.get('/api/topic/:tId/level/:lId', routes.auth.authSilent, api.tutorial.levelGet);
app.get('/api/topic/:tId/level/:lId/ex', routes.auth.authSilent, api.tutorial.exerciseList);
app.get('/api/topic/:tId/level/:lId/ex/:exId', routes.auth.authSilent, api.tutorial.exerciseGet);

var server = http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});

var io = socketio.listen(server);

io.set('authorization', passportSIO.authorize({
    key: 'connect.sid',
    secret: nconf.get('session_secret'),
    store: new MongoStore({
        db: nconf.get('database:name')
    }),
    fail: function (data, accept) {
        accept(null, false);
    },
    success: function (data, accept) {
        accept(null, true);
    }
}));

io.on('connection', function (socket) {
    socket.on('runCode', function (codeId) {
        api.run(socket, codeId);
    });
});
