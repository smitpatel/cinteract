var compiler = require('../lib/compile.js');
var runner = require('../lib/run.js');
var User = require('../models/user.js');
var tutorial = require('./tutorial.js');

exports.compile = compiler.compile;
exports.run = runner.run;
exports.tutorial = tutorial;

exports.checkAvailable = function(req, res) {
    User.userExists(req.body.email, function(exist) {
	if (exist) {
	    res.send(false);
	} else {
	    res.send(true);
	}
    });
};
