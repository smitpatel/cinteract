var Topic = require('../models/topic.js'),
    Level = require('../models/level.js');

exports.levelGet = function (req, res) {
    var queryObj = { _id: req.params.tId };
    queryObj["levels." + req.params.lId] = { $exists: true };

    Topic.findOne(queryObj, { levels: 1 }, function (err, topic) {
        console.log(topic);
        if (err) {
            // TODO: use logger
            console.log(err);
            res.send({ status: 'failure', error: 'Unknown error.'});
        } else if (topic) {
            res.send(topic.levels[req.params.lId]);
        } else {
            res.send({ status: 'failure', error: 'Invalid level offset.'});
        }
    });
};

exports.levelList = function (req, res) {
    Topic.findById(req.params.tId, function (err, topic) {
        console.log(topic);
        if (err) {
            // TODO: use logger
            console.log(err);
            res.send({ status: 'failure', error: 'Unknown error.'});
        } else if (topic.levels !== undefined) {
            res.send(topic.levels);
        } else {
            res.send({ status: 'failure', error: 'Invalid topic ID.' });
        }
    });
};

exports.exerciseGet = function (req, res) {
    var queryObj = { _id: req.params.tId };
    queryObj["levels." + req.params.lId] = { $exists: true };

    Topic.findOne(queryObj, function (err, topic) {
        if (err) {
            // TODO: use logger
            console.log(err);
            res.send({ status: 'failure', error: 'Invalid topic ID.' });
        } else if (topic) {
            // res.send(topic.levels);
            Level.populate(topic.levels[req.params.lId], {
                path: 'exercises'
                // match: queryObj
                // select: '-_id'
            }, function (err, level) {
                if (err) {
                    console.log(err);
                    res.send({ status: 'failure', error: 'Unknown error.'});
                } else if (level.exercises[req.params.exId]) {
                    res.send(level.exercises[req.params.exId]);
                } else {
                    res.send({ status: 'failure', error: 'Invalid exercise ID.' });
                }
            });
        } else {
            res.send({ status: 'failure', error: 'Invalid topic ID.' });
        }
    });
};

exports.exerciseList = function (req, res) {
    var queryObj = { _id: req.params.tId };
    queryObj["levels." + req.params.lId] = { $exists: true };

    Topic.findOne(queryObj, function (err, topic) {
        if (err) {
            // TODO: use logger
            console.log(err);
            res.send({ status: 'failure', error: 'Invalid topic ID.' });
        } else if (topic) {
            // res.send(topic.levels);
            Level.populate(topic.levels[req.params.lId], {
                path: 'exercises'
                // select: '-_id'
            }, function (err, level) {
                if (err) {
                    console.log(err);
                    res.send({ status: 'failure', error: 'Unknown error.'});
                } else {
                    res.send(level.exercises);
                }
            });
        } else {
            res.send({ status: 'failure', error: 'Invalid topic ID.' });
        }
    });
};

// TODO: find queryObj soln. It's hack and ugly js
// TODO: Invalid exercise ID err is pointless. Always returning unknown err
