var auth = require('./auth.js');
var signup = require('./signup.js');
var user = require('./user.js');

exports.login = function(req, res) {
    if (req.isAuthenticated()) {
        res.redirect('/home');
    } else {
      res.render('login', { user: req.user, title: 'Login', flash: req.flash() });
    }
};

exports.index = function(req, res) {
    res.render('index', { title: 'Cinteract Home' });
};

exports.home = exports.course = function(req, res) {
    res.render('home', { title: 'Welcome' });
};

exports.templates = function (req, res) {
    var name = req.params.name;
    res.render('level/' + name, {title: 'Welcome'});
};

//Exported controller modules
exports.auth = auth;
exports.signup = signup;
exports.user = user;
