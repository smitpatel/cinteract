var User = require('../models/user.js');
var cbcrypt = require('../lib/cbcrypt.js');

exports.Add = function(req, res) {
    var errors, exist;
    req.assert('fname', 'Invalid fname').notEmpty(1, 64);
    req.assert('email', 'Invalid email').isEmail();
    req.assert('password', 'Password does not match').equals(req.body.cpassword);
    
    errors = req.validationErrors();
    if (errors) {
        //res.render('signup', { title:'Signup - Cinteract', errMsg: "Invalid signup details" });
        var msgs = [];
        for (i in errors) {
            msgs.push( { msg: errors[i].msg, param: errors[i].param } );
        }
        res.send( { status: "failure", errMsg: "Invalid signup details", error: msgs } );
        return;
    };
    
    exist = User.userExists(req.body.username, function (ret) {
        // res.render('signup', {
            // title: 'Signup - Cinteract',
            // errMsg: 'Already registered' 
        // });
        if (ret) {
            res.send( { status: "failure", errMsg: "User already exist." } );
        }
    });

    if (exist) {
        return;
    }
    
    cbcrypt.getPasswordHash(req.body.password, function (passwordHash){
        var user = new User({ 
            fname: req.body.fname,
            lname: req.body.lname,
            username: req.body.username,
            email: req.body.email,
            password: passwordHash
        });
        
        user.save(function (err) {
            if (err) {
                console.log(err);
                //TODO: add to logger
            }
        });
        res.send( { uid: user.id, status: "success" } );
    }); 
};

exports.index = function (req, res) {
    res.render('signup', { title: 'Signup' });
};
