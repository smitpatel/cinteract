exports.auth = function ensureAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
};

exports.authSilent = function ensureAuthSilent(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.send({ error: "Not authenticated. Login first." });
};
