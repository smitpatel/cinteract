exports.progress = function (req, res) {
    res.render('profile/progress', {title: 'Welcome', canEdit: false});
};

exports.badge = function (req, res) {
    res.render('profile/badge', {title: 'Welcome', canEdit: false});
};

exports.settings = function (req, res) {
    res.render('profile/settings', {title: 'Welcome', canEdit: false});
};

exports.about = function (req, res) {
    res.render('profile/about', {title: 'Welcome', canEdit: false});
};

exports.user = function(req, res) {
    res.render('profile', { title: 'Welcome', canEdit: false, hasOwnImage: false });
};
