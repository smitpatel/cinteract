function LevelViewCtrl($scope) {
  $scope.topicid = 3;
  $scope.topicname = "Introduction to Datatypes";
  $scope.levelid = 2;
  $scope.levelname = "Integers";
  $scope.levelLen = 4;
  $scope.levels = [];
  $scope.prevurl = "#";
  $scope.prevlabel = "\u2190 Previous";
  $scope.nexturl = "#";
  $scope.nextlabel = "Next \u2192";
  
  for (i=1; i<=$scope.levelLen; i++) {
   $scope.levels.push({id: i, url: "#topic/"+$scope.topicid+"/level/"+i });
  }
}

function ProfileAboutViewCtrl($scope) {
  $scope.username = "erione";
  $scope.fname = "Brijesh";
  $scope.lname = "Patel";
  $scope.email = "erione@gmail.com";
  $scope.location = "Ahmedabad";
  $scope.education = "Undergrad";
  $scope.institute = "DA-IICT";
}

function ProfileProgressViewCtrl($scope) {
  $scope.progress = "27%";
  $scope.topics = [];
  
  $scope.topic = { id:"var12", name: "Variables", progress: "40%", 
		      levels: [{ name: "Introduction to variables", progress: "100%" },
			       { name: "Storing values in variables", progress: "70%" },
			       { name: "Scope of a variable", progress: "25%" },
			       { name: "Static Variables", progress: "40%" }
		      ] };
  
  $scope.topics.push($scope.topic);
  $scope.topics.push({ id:"dat12", name: "Datatypes", progress: "30%", levels: [] });
  $scope.topics.push({ id:"opr12", name: "Operators", progress: "15%", levels: [] });
  $scope.topics.push({ id:"str12", name: "Strings", progress: "60%", levels: [] });
}

function ProfileBadgeViewCtrl($scope) {
  
}