var id, iosocket, runCode, compileCode, sendInput, startPrompt, codeEditor, jqconsole;

compileCode = function() {
  rawcode = codeEditor.getValue();

  $.post('/api/compile', {'src': rawcode}, function(a) {
    out = a.compile_op;
    if (!out)
      out = "Compiled successfully.\n";
    jqconsole.Write(out);
    id = a.executable;
    startPrompt();
  });
}

runCode = function() {
  if(id) {
    $.getScript('socket.io/socket.io.js');
    iosocket = io.connect();

    iosocket.emit('runCode', id);
    jqconsole.Input(function(input) { sendInput(input); });

    iosocket.on('error', function(reason) {
//       jqconsole.Write(reason);  // highlight run time errors
    });

    iosocket.on('message', function(message) {
      jqconsole.Write(message);
      jqconsole.Focus();
    });

    iosocket.on('exit', function (code) {
      iosocket.disconnect();
      jqconsole.RestartPrompt();
      startPrompt();
    });
  } else {
    jqconsole.Write("Compile the code first.\n");
    startPrompt();
  }
}

sendInput = function(data) {
  iosocket.send(data + "\n");
  jqconsole.Input(function(input) { sendInput(input); });
}

startPrompt = function () {
  jqconsole.Prompt(true, function (command) {
    if (command) {
      try {
	switch(command)
	{
	case 'clear':
	  jqconsole.Reset();startPrompt();
	  break;
	case 'compile':
	  compileCode();
	  break;
	case 'run':
	  runCode();
	  break;
	case 'help':
	  jqconsole.Write('Commands list :\n   compile : Compiles your code\n   run : Runs your compiled code\n   clear : Clears the console\n');
	  startPrompt();
	  break;
	default:
	  jqconsole.Write('Command not found. Type \'help\' to see list of all commands.\n');
	  startPrompt();
	  break;
	}
      } catch (e) {
	jqconsole.Write('ERROR: ' + e.message + '\n');
      }
    } else {
      startPrompt();
    }
  });
}
