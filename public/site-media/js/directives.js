// Sub routes
var appCourse = angular.module('cint-course', []);

appCourse.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/topic', {templateUrl: 'templates/level', controller: LevelViewCtrl}).
//       when('/course/topic/:topicid', {templateUrl: 'templates/topics.ejs', controller: TopicDetailCtrl}).
    otherwise({redirectTo: '/topic'});
}]);

var appProfile = angular.module('cint-profile', []);

appProfile.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/about', {templateUrl: 'part/profile/about', controller: ProfileAboutViewCtrl}).
    when('/progress', {templateUrl: 'part/profile/progress', controller: ProfileProgressViewCtrl}).
    when('/badges', {templateUrl: 'part/profile/badge', controller: ProfileBadgeViewCtrl}).
    otherwise({redirectTo: '/progress'});
}]);


// Directives
appCourse.directive('editor', function () {
  return {
    restrict:'A',
    link:function (scope, elm, attrs) {
      var startCodeEditor, codeEditor;

      startCodeEditor = function () {
	codeEditor = CodeMirror.fromTextArea(elm[0], { 
	  lineNumbers: true,
	  matchBrackets: true,
	  mode: "text/x-csrc",
	  indentUnit: 4,
	  tabSize: 4,
	  onCursorActivity: function() {
	    codeEditor.setLineClass(hlLine, null, null);
	    codeEditor.setMarker(hlLine);
	    hlLine = codeEditor.setLineClass(codeEditor.getCursor().line, null, "activeline");
	    codeEditor.setMarker(hlLine, "%N%", "activemarker");
	  }
	});
	
	var hlLine = codeEditor.getCursor().line;
      };
      startCodeEditor();
      window.codeEditor = codeEditor;
    }
  };
});

appCourse.directive('console', function () {
  return {
    restrict:'A',
    link:function (scope, elm, attrs) {
      var jqconsole, startConsole;
      
      startConsole = function() {
	var header = 'Welcome to the world of programming. Welcome to CInteract.\n\n';
	
	jqconsole = elm.jqconsole(header, 'user@cinteract$ ');
	window.jqconsole = jqconsole;

	jqconsole.RegisterShortcut('C', function() {
	  jqconsole.AbortPrompt();
	  startPrompt();
	});
	
	jqconsole.RegisterMatching('{', '}', 'brace');
	jqconsole.RegisterMatching('(', ')', 'paran');
	jqconsole.RegisterMatching('[', ']', 'bracket');
	
	startPrompt();
      }
      startConsole();
    }
  };
});

