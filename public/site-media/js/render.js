var render = function(xmlText) {
  if (window.DOMParser)
  {
    parser=new DOMParser();
    var xmlDoc=parser.parseFromString(xmlText,"text/xml");
  } else {
    // Internet Explorer
    var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async=false;
    xmlDoc.loadXML(xmlText); 
  }
  htmlText = parseXmlToHtml(xmlDoc);
  return htmlText;
}

var parseXmlToHtml = function(xmlDoc) {
  var html='<span class="tut-act">';
  
  root = xmlDoc.getElementsByTagName("tutorial")[0];
  node = root.firstChild;
  
  while (node != null) {
    switch (node.nodeName) {
      case "code":
	html += '<code>'
	cNode = node.firstChild;
	while (cNode != null) {
	  switch (cNode.nodeName) {
	    case "var":
	      html += '<span class="code-var">' + cNode.firstChild.nodeValue + '</span>';
	      break;
	    
	    case "type":
	      html += '<span class="code-type">' + cNode.firstChild.nodeValue + '</span>';
	      break;
	    
	    case "val":
	      html += '<span class="code-val">' + cNode.firstChild.nodeValue + '</span>';
	      break;
	    
	    case "op":
	      html += '<span class="code-op">' + cNode.firstChild.nodeValue + '</span>';
	      break;
	    
	    default:
	      html += cNode.nodeValue;
	  }
	  cNode = cNode.nextSibling;
	}
	html += '</code>';
	break;

      case "ol":
      case "ul":
	name = node.nodeName;
	html += '<'+name+'>';
	cNode = node.firstChild;
	while (cNode != null) {
	  html += '<li>' + cNode.firstChild.nodeValue + '</li>';
	  cNode = cNode.nextSibling;
	}
	html += '</'+name+'>';
	break;
	    
      case "#text":
	html += node.nodeValue;
	break;
	
      case "br":
	html += '<br>';
	break;
	
      default:
	name = node.nodeName;
	val = (node.firstChild.nodeValue != null) ? node.firstChild.nodeValue : "";
	html += '<'+name+'>' + val + '</'+name+'>';
    }
    node = node.nextSibling;
  }
  html += '</span>';
  return html;
}